console.log('Hello World')

// ACTIVITY 1
let username; 
let password;
let role;

function logIn() {

	username = prompt('Enter username')
	password = prompt('Enter password')
	role = prompt('Enter role').toLowerCase()

	if (username == "" || username == null) {
		alert ("Please input valid username")
	}

	else if (password == "" || password == null) {
		alert ("Please input valid password")
	} 

	else if (role == "" || role == null) {
		alert ("Please input valid roll")
	} 

	else {
		switch (role) {

			case 'admin':
			console.log('Welcome back to the class portal, Admin!')
			break;

			case 'teacher':
			console.log('Thank you for logging in, teacher!')
			break;

			case 'student':
			console.log('Welcome to the class portal, student!')
			break;

			default:
			console.log('Role out of range')
			break;

		}
	}
}
logIn()







// ACTIVITY 2
let grade;
function checkAverage(num1, num2, num3, num4) {
	let average = Math.round((num1 + num2 +num3 + num4)/4)

	if (average <= 74) {
		grade = 'F'
		console.log(`Hello, student, you average is ${average}. The equivalent is ${grade}`)
				
	} else if (average>=75 && average<=79) {
		grade = 'D'
		console.log(`Hello, student, you average is ${average}. The equivalent is ${grade}`)
		
	} else if (average>=80 && average<=84) {
		grade = 'C'
		console.log(`Hello, student, you average is ${average}. The equivalent is ${grade}`)
		
	} else if (average>=85 && average<=89) {
		grade = 'B'
		console.log(`Hello, student, you average is ${average}. The equivalent is ${grade}`)
		
	} else if (average>=90 && average<=95) {
		grade = 'A'
		console.log(`Hello, student, you average is ${average}. The equivalent is ${grade}`)
		
	} else if (average>=96) {
		grade = 'A+'
		console.log(`Hello, student, you average is ${average}. The equivalent is ${grade}`)
		
	}
}
console.log('checkAverage(70,70,72,71)')
console.log(checkAverage(70,70,72,71))

console.log('checkAverage(76,76,77,79)')
console.log(checkAverage(76,76,77,79))

console.log('checkAverage(81,83,84,85)')
console.log(checkAverage(81,83,84,85))

console.log('checkAverage(87,88,88,89)')
console.log(checkAverage(87,88,88,89))

console.log('checkAverage(91,90,92,90)')
console.log(checkAverage(91,90,92,90))

console.log('checkAverage(96,95,97,97)')
console.log(checkAverage(96,95,97,97))